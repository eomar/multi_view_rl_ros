// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#include "multi_view_rl_ros/cartesian_pose_controller.h"

#include <controller_interface/controller_base.h>
#include <franka_hw/franka_cartesian_command_interface.h>
#include <hardware_interface/hardware_interface.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>

#include <Eigen/Dense>
#include <algorithm>
#include <cmath>
#include <memory>
#include <stdexcept>
#include <string>

#include "multi_view_rl_ros/CartesianMotionCommand.h"

namespace multi_view_rl_ros {

bool CartesianPoseController::execute_cartesian_motion(
    multi_view_rl_ros::CartesianMotionCommand::Request& request,
    multi_view_rl_ros::CartesianMotionCommand::Response& response) {
  // Resets the trajectory before specifying a new desired pose.
  // NOTE: This also initializes the desired pose from the previous desired
  // pose.
  reset_trajectory(true);

  // Verifies that the desired axis of motion is valid.
  auto it = std::find(kValidAxesOfMotion.begin(), kValidAxesOfMotion.end(),
                      request.axis_of_motion);
  if (it == kValidAxesOfMotion.end()) {
    ROS_WARN("The requested motion is along an invalid axis.");
    return false;
  }

  // TODO: Perhaps I could add a workspace check here as well. Or clipping.

  // Checks that the desired action magnitude is within the limits.
  if (request.axis_of_motion != 3) {
    if (std::abs(request.action_magnitude) > kMaxActionMovementDistance) {
      ROS_WARN("The requested translation is too large.");
      return false;
    }
  } else {
    // NOTE: The rotation magnitude is assumed to be in radians.
    if (std::abs(request.action_magnitude) > kMaxActionRotationAngle) {
      ROS_WARN("The requested rotation is too large.");
      return false;
    }
  }

  // TODO: Create a script that transforms the discrete action into an axis of
  // motion and signed action magnitude.

  // Updates the desired motion parameters.
  axis_of_motion_ = request.axis_of_motion;
  action_magnitude_ = request.action_magnitude;

  response.action_magnitude = request.action_magnitude;
  return true;
}

bool CartesianPoseController::init(hardware_interface::RobotHW* robot_hardware,
                                   ros::NodeHandle& node_handle) {
  // Initializes the cartesian motion service.
  cartesian_motion_service_ = node_handle.advertiseService(
      "execute_cartesian_motion",
      &CartesianPoseController::execute_cartesian_motion, this);

  cartesian_pose_interface_ =
      robot_hardware->get<franka_hw::FrankaPoseCartesianInterface>();
  if (cartesian_pose_interface_ == nullptr) {
    ROS_ERROR(
        "CartesianPoseController: Could not get Cartesian Pose "
        "interface from hardware");
    return false;
  }

  std::string arm_id;
  if (!node_handle.getParam("arm_id", arm_id)) {
    ROS_ERROR("CartesianPoseController: Could not get parameter arm_id");
    return false;
  }

  try {
    cartesian_pose_handle_ =
        std::make_unique<franka_hw::FrankaCartesianPoseHandle>(
            cartesian_pose_interface_->getHandle(arm_id + "_robot"));
  } catch (const hardware_interface::HardwareInterfaceException& e) {
    ROS_ERROR_STREAM(
        "CartesianPoseController: Exception getting Cartesian handle: "
        << e.what());
    return false;
  }

  auto state_interface = robot_hardware->get<franka_hw::FrankaStateInterface>();
  if (state_interface == nullptr) {
    ROS_ERROR(
        "CartesianPoseController: Could not get state interface from "
        "hardware");
    return false;
  }

  // TODO: Make sure that the initial pose has the end-effector pointing
  // directly towards the table.

  return true;
}

void CartesianPoseController::reset_trajectory(bool start_new_trajectory) {
  new_trajectory_ = start_new_trajectory;
  elapsed_time_ = ros::Duration(0.0);

  // Resets the different poses of the trajectory.
  // TODO: Should this be the current pose instead since this is where the
  // robot is at the time a new desired pose is specified?
  initial_pose_ = cartesian_pose_handle_->getRobotState().O_T_EE_d;
  intermediate_pose_ = initial_pose_;

  // Commands the robot to keep its pose.
  cartesian_pose_handle_->setCommand(initial_pose_);

  // Resets the axis of motion to disallow invalid movements.
  axis_of_motion_ = -1;
  action_magnitude_ = 0.0;
}

void CartesianPoseController::starting(const ros::Time& /* time */) {
  reset_trajectory(false);
}

void CartesianPoseController::update(const ros::Time& /* time */,
                                     const ros::Duration& period) {
  // Sends commands to the controller only if a new trajectory has been started.
  if (new_trajectory_) {
    // Updates the desired pose setpoint only if the axis of motion is valid.
    auto it = std::find(kValidAxesOfMotion.begin(), kValidAxesOfMotion.end(),
                        axis_of_motion_);
    if (it == kValidAxesOfMotion.end()) {
      ROS_WARN("Invalid axis of motion. The robot end-effector will not move.");
      reset_trajectory(false);
      return;
    }

    // Updates the elapsed time along the current trajectory.
    // NOTE: The period here is 0.001 s corresponding to a 1 kHz control rate
    // of the robot arm.
    elapsed_time_ += period;

    // Resets the trajectory as soon as the elapsed time exceeds the time
    // constant.
    if (elapsed_time_.toSec() > kTimeConstant) {
      reset_trajectory(false);
      return;
    }

    // Computes the intermediate pose along the current minimum jerk
    // trajectory.
    // TODO: Tune the time constant such that motion commands can be executed
    // sequentially.
    // TODO: Verify that the same time constant is also suitable for
    // rotations.
    // TODO: Why do lower time constants not work? Are there limit
    // violations?
    double dt = elapsed_time_.toSec() / kTimeConstant;
    double dt2 = dt * dt;
    double dt3 = dt * dt2;
    double dt4 = dt2 * dt2;
    double dt5 = dt4 * dt;

    // Distinguishes translations from rotations and handles the pose update
    // accordingly.
    if (axis_of_motion_ != 3) {
      intermediate_pose_map_(axis_of_motion_, 3) =
          initial_pose_map_(axis_of_motion_, 3) +
          action_magnitude_ * (10 * dt3 - 15 * dt4 + 6 * dt5);

    } else {
      // Creates the relative rotation around the z-axis with the desired
      // magnitude.
      // NOTE: The applied rotation is considered relative to the initial
      // orientation. Therefore, the initial relative angle is 0.
      const double intermediate_angle =
          action_magnitude_ * (10 * dt3 - 15 * dt4 + 6 * dt5);

      const Eigen::Matrix3d relative_rotation_around_z =
          Eigen::AngleAxisd(intermediate_angle, Eigen::Vector3d::UnitZ())
              .toRotationMatrix();

      // NOTE: Since the rotation is relative to the initial pose it has to be
      // multiplied with the initial orientation.
      intermediate_pose_map_.topLeftCorner<3, 3>() =
          initial_pose_map_.topLeftCorner<3, 3>() * relative_rotation_around_z;
    }

    // Sends the desired cartesian pose to the low-level controller.
    // TODO: Does this have to be sent at every time step? If so, move outside
    // the if statement.
    // TODO: Do not actually send any commands when testing this at first.
    cartesian_pose_handle_->setCommand(intermediate_pose_);
  }
}

}  // namespace multi_view_rl_ros

PLUGINLIB_EXPORT_CLASS(multi_view_rl_ros::CartesianPoseController,
                       controller_interface::ControllerBase)
