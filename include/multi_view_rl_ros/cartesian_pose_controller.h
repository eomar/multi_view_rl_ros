// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#pragma once

#include <controller_interface/multi_interface_controller.h>
#include <franka_hw/franka_cartesian_command_interface.h>
#include <franka_hw/franka_state_interface.h>
#include <hardware_interface/robot_hw.h>
#include <ros/node_handle.h>
#include <ros/time.h>

#include <Eigen/Dense>
#include <array>
#include <memory>
#include <string>

#include "multi_view_rl_ros/CartesianMotionCommand.h"

namespace multi_view_rl_ros {

// The bounds for the maximum action magnitudes (rotation and translation).
// TODO: Tune these action bounds.
constexpr double kMaxActionMovementDistance = 0.03;
constexpr double kMaxActionRotationAngle = 0.174;

// The minimum jerk trajectory time constant.
// TODO: If the length of the segments would change, perhaps I should compute
// this adaptively instead (e.g. assuming an average velocity).
constexpr double kTimeConstant = 1.0;

// The set of valid axes, where '3' represents the rotation around the gripper's
// z-axis.
constexpr std::array<int, 4> kValidAxesOfMotion = {{0, 1, 2, 3}};

class CartesianPoseController
    : public controller_interface::MultiInterfaceController<
          franka_hw::FrankaPoseCartesianInterface,
          franka_hw::FrankaStateInterface> {
 public:
  CartesianPoseController()
      : initial_pose_map_(initial_pose_.data()),
        intermediate_pose_map_(intermediate_pose_.data()) {}

  bool execute_cartesian_motion(
      multi_view_rl_ros::CartesianMotionCommand::Request& req,
      multi_view_rl_ros::CartesianMotionCommand::Response& res);

  bool init(hardware_interface::RobotHW* robot_hardware,
            ros::NodeHandle& node_handle) override;

  void starting(const ros::Time&) override;
  void update(const ros::Time&, const ros::Duration& period) override;

 private:
  franka_hw::FrankaPoseCartesianInterface* cartesian_pose_interface_;
  std::unique_ptr<franka_hw::FrankaCartesianPoseHandle> cartesian_pose_handle_;

  ros::ServiceServer cartesian_motion_service_;
  ros::Duration elapsed_time_;
  std::array<double, 16> initial_pose_{};
  std::array<double, 16> intermediate_pose_{};

  // These Eigen::Map objects make it easier to manipulate the transformation
  // matrices.
  Eigen::Map<Eigen::Matrix4d> initial_pose_map_;
  Eigen::Map<Eigen::Matrix4d> intermediate_pose_map_;

  bool new_trajectory_;
  int axis_of_motion_;
  double action_magnitude_;

  // Resets the trajectory to keep the robot end-effector at the current pose.
  void reset_trajectory(bool start_new_trajectory);
};

}  // namespace multi_view_rl_ros
