#!/usr/bin/env python
import actionlib
import control_msgs.msg
import franka_control.msg
import franka_gripper.msg
import moveit_commander
from moveit_commander.conversions import list_to_pose
from moveit_msgs.msg import MoveGroupAction
import rospy


class GripperController(object):
    def __init__(self):
        self.grasp_client = actionlib.SimpleActionClient(
            "/franka_gripper/grasp", franka_gripper.msg.GraspAction
        )
        self.grasp_client.wait_for_server()
        self.open_gripper = rospy.get_param("~open_gripper")

    def grasp(self, width=0.0, e_inner=0.1, e_outer=0.1, speed=0.1, force=10.0):
        epsilon = franka_gripper.msg.GraspEpsilon(e_inner, e_outer)
        goal = franka_gripper.msg.GraspGoal(width, epsilon, speed, force)
        self.grasp_client.send_goal(goal)
        return self.grasp_client.wait_for_result(rospy.Duration(2.0))


if __name__ == '__main__':
    rospy.init_node('gripper_controller', anonymous=False)
    controller = GripperController()
    if controller.open_gripper:
        controller.grasp(width=1.0)
    else:
        controller.grasp(width=0.0)
