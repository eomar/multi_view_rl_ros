#!/usr/bin/env python

# TODO: Modify this script to publish desired end-effector pose commands.

# First, this has to subscribe to the robot state. Then it should modify the
# end-effector pose and publish the new desired pose on the
# "panda/desired_ee_pose" topic.

import franka_msgs.msg
from multi_view_rl_ros.msg import DesiredEndEffectorPose
from multi_view_rl_ros.srv import CartesianMotionCommand
import rospy
from std_msgs.msg import String

class DesiredPoseCommander(object):
    def __init__(self):
        # Initializes subscribers and publishers.
        rospy.Subscriber(
                "/franka_state_controller/franka_states",
                franka_msgs.msg.FrankaState,
                self.robot_state_cb,
                queue_size=1,
            )

        self.previous_O_T_EE_d = None
        self.count = 0

    def robot_state_cb(self, msg):
        self.previous_O_T_EE_d = msg.O_T_EE_d

    def run(self):
        rate = rospy.Rate(1)
        while not rospy.is_shutdown():
            # Requests a motion along the same axis in alternating fashion.
            if self.count % 2 == 0:
                sign = 1
            else:
                sign = -1

            self.count += 1

            # TODO: Send the action via the service.
            axis_of_motion = 0 
            action_magnitude = sign * 0.01
            self._send_command_service(axis_of_motion, action_magnitude)

            rospy.loginfo('Current action %d', axis_of_motion)

            rate.sleep()

    def _send_command_service(self, axis_of_motion, action_magnitude):
        rospy.wait_for_service('/cartesian_pose_controller/execute_cartesian_motion')
        try:
            send_desired_action = rospy.ServiceProxy(
                '/cartesian_pose_controller/execute_cartesian_motion',
                CartesianMotionCommand)

            response = send_desired_action(axis_of_motion, action_magnitude)
            return response

        except rospy.ServiceException as e:
            print("Service call failed: %s"%e)

if __name__ == '__main__':
    rospy.init_node('desired_pose_publisher', anonymous=False)
    commander = DesiredPoseCommander()
    commander.run()
