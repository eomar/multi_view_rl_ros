#!/usr/bin/env python


import cv2
import cv_bridge
import franka_msgs.msg
import geometry_msgs.msg
import numpy as np
import rospy
import sensor_msgs.msg
from supereight_wrapper import DenseSLAMSystemWrapper, depthBlockAveraging

# TODO: Add these to the helper_function.py file later.


def invert_transformation(T_A_B):
    assert len(T_A_B.shape) == 2
    assert T_A_B.shape[0] == 4
    assert T_A_B.shape[1] == 4
    T_B_A = np.zeros((4, 4))
    T_B_A[:3, :3] = T_A_B[:3, :3].T
    T_B_A[:3, 3] = - np.matmul(T_A_B[:3, :3].T, T_A_B[:3, 3])
    T_B_A[3, 3] = 1.0
    return T_B_A


def quaternion_to_rotation_matrix(q_A_B):
    # NOTE: This assumes the quaternion is in the [q_x, q_y, q_z, q_w] format.
    assert len(q_A_B) == 4
    norm = np.linalg.norm(q_A_B)
    if norm != 1.0:
        q_A_B = list(np.array(q_A_B) / norm)

    q_x = q_A_B[0]
    q_y = q_A_B[1]
    q_z = q_A_B[2]
    q_w = q_A_B[3]

    R_A_B = np.empty((3, 3))
    R_A_B[0, 0] = q_w**2 + q_x**2 - q_y**2 - q_z**2
    R_A_B[0, 1] = 2 * q_x * q_y - 2 * q_w * q_z
    R_A_B[0, 2] = 2 * q_w * q_y + 2 * q_x * q_z
    R_A_B[1, 0] = 2 * q_w * q_z + 2 * q_x * q_y
    R_A_B[1, 1] = q_w**2 - q_x**2 + q_y**2 - q_z**2
    R_A_B[1, 2] = 2 * q_y * q_z - 2 * q_w * q_x
    R_A_B[2, 0] = 2 * q_x * q_z - 2 * q_w * q_y
    R_A_B[2, 1] = 2 * q_w * q_x + 2 * q_y * q_z
    R_A_B[2, 2] = q_w**2 - q_x**2 - q_y**2 + q_z**2
    return R_A_B


def rotation_matrix_to_quaternion(R_A_B):
    # Returns the equivalent quaternion in the [q_x, q_y, q_z, q_w] format.
    assert len(R_A_B.shape) == 2
    assert R_A_B.shape[0] == 3
    assert R_A_B.shape[1] == 3
    q_A_B = [None] * 4
    q_A_B[0] = (0.5 * np.sign(R_A_B[2, 1] - R_A_B[1, 2]) *
                np.sqrt(R_A_B[0, 0] - R_A_B[1, 1] - R_A_B[2, 2] + 1))
    q_A_B[1] = (0.5 * np.sign(R_A_B[0, 2] - R_A_B[2, 0]) *
                np.sqrt(R_A_B[1, 1] - R_A_B[2, 2] - R_A_B[0, 0] + 1))
    q_A_B[2] = (0.5 * np.sign(R_A_B[1, 0] - R_A_B[0, 1]) *
                np.sqrt(R_A_B[2, 2] - R_A_B[0, 0] - R_A_B[1, 1] + 1))
    q_A_B[3] = 0.5 * np.sqrt(R_A_B[0, 0] + R_A_B[1, 1] + R_A_B[2, 2] + 1)
    return q_A_B


def pose_to_transformation_matrix(pose):
    assert len(pose) == 7
    # The pose is assumed to have the following format:
    # [x, y, z, q_x, q_y, q_z, q_w].
    T_AB = np.zeros((4, 4))
    T_AB[:3, 3] = pose[:3]
    T_AB[:3, :3] = quaternion_to_rotation_matrix(pose[3:])
    T_AB[3, 3] = 1.0
    return T_AB


class DepthImageProcessor(object):
    def __init__(self):
        self.cv_bridge = cv_bridge.CvBridge()

        # TODO: Throttle the depth image input to 1 Hz. Or only for the
        # inference.

        # The queue size is 1 to always use the latest image.
        rospy.Subscriber('/camera/depth/image_rect_raw',
                         sensor_msgs.msg.Image,
                         self.depth_image_callback, queue_size=1)

        rospy.Subscriber('/franka_state_controller/franka_states',
                         franka_msgs.msg.FrankaState,
                         self.robot_state_callback, queue_size=1)

        self.raycast_image_publisher = rospy.Publisher(
            'raycast_image', sensor_msgs.msg.Image, queue_size=1)

        # TODO: Clean this up.
        # Raycasting parameters.
        self.image_width = 64
        self.image_height = 64
        self.perspective_angle = 85.0 * np.pi / 180.0

        # TODO: Specify the raycast view poses as relative transformations
        # from depth camera.

        # Computes the transformation from the end-effector to the camera,
        # EE_T_C.
        # NOTE: Since the hand-eye calibration yields the transformation between
        # the 8th link of the panda arm and the camera, the transformation from
        # the end-effector to the camera must be computed.
        # NOTE: These values are hardcoded and should remain constant.
        panda_link8_T_C_pose = [0.03226862, -0.06137175,
                                0.04107702, 0.00099995,
                                0.0, 0.39898185, 0.91695828]
        panda_link8_T_EE_pose = [0.0, 0.0, 0.10339999944, 0.0,
                                 0.0, -0.38268051139, 0.92387769054]

        # Converts the poses to transformation matrices.
        panda_link8_T_C_matrix = pose_to_transformation_matrix(
            panda_link8_T_C_pose)

        panda_link8_T_EE_matrix = pose_to_transformation_matrix(
            panda_link8_T_EE_pose)

        # Inverts the transformation matrix.
        EE_T_panda_link8_matrix = invert_transformation(
            panda_link8_T_EE_matrix)

        self.EE_T_C_matrix = np.matmul(EE_T_panda_link8_matrix,
                                       panda_link8_T_C_matrix)

        # TODO: Make sure the robot starts from this position?
        # Or does it not matter because this just defines the center of the
        # mapped volume roughly?
        # NOTE: These values come from O_T_EE at the franka examples starting
        # pose.
        self.initial_robot_position = [0.307, 0, 0.486]

        self.first_iteration = True
        self.filtering_cutoff_depth = 0.3

        # This is used to prevent the callback from trying to integrate an
        # image into the map before receiving any pose messages.
        self.received_first_pose = False

        # Initializes the SLAM setup using the depth camera intrinsics and
        # the hand-eye calibration.
        self._initialize_slam()

    def depth_image_callback(self, msg):
        # Integrates the latest depth image into the SLAM map and computes the
        # desired end-effector pose

        # Converts the image message to a float32 numpy array with depth values
        # in meters.
        # NOTE: The input image has the following type: 16UC1.
        depth_image = self.cv_bridge.imgmsg_to_cv2(
            msg).astype(np.float32) * 0.001

        # TODO: Check that the filter actually removes the peg out of the
        # image.

        # Computes the mask that filters out the peg and the gripper's fingers
        # in the first iteration.
        if self.first_iteration:
            self.filtering_mask = depth_image < self.filtering_cutoff_depth

            # Dilates the mask to remove possible errors due to depth image
            # discretization.
            structuring_element = cv2.getStructuringElement(
                cv2.MORPH_CROSS, (3, 3))
            self.filtering_mask = cv2.dilate(
                self.filtering_mask.astype('uint8'),
                structuring_element).astype(bool)

            # # Downsamples the mask to use it with the virtual raycast views.
            # self.downsampled_filtering_mask = crop_to_square(
            #     self.filtering_mask, 448)[::7, ::7]

            self.first_iteration = False

        # Filters out the peg and the gripper's fingers from the depth image.
        depth_image[self.filtering_mask] = 0.0

        if self.received_first_pose:
            self._integrate_into_map(depth_image)

        # Raycasts an image from the same pose as the depth camera and
        # publishes it to visualize the mapping.
        raycast_image = self._raycast_image()
        # Converts from np.float32 to np.uint16 such that the message is
        # interpreted as depth.
        raycast_image = (raycast_image * 1000).astype(np.uint16)
        raycast_image_msg = self.cv_bridge.cv2_to_imgmsg(raycast_image)
        self.raycast_image_publisher.publish(raycast_image_msg)

        # TODO: Crop and downsample the image to 64x64.
        # Note that the image is now 640x480.
        # Crop to 448x448 and downsample with a factor of 7.
        # NOTE: The Realsense developers recommend downsampling with the
        # non-zero block averaging.

    def _get_camera_pose_in_base_frame(self):
        # Assumes the pose is represented as a flattened transformation matrix
        # (column major) in the robot base frame and returns a pose (with
        # quaternions) in the map frame.
        assert self.received_first_pose
        assert len(self.O_T_EE) == 16

        # Unflattens the transformation matrix to a 4x4 matrix while respecting
        # the column-major format.
        O_T_EE_matrix = np.array(
            self.O_T_EE, dtype=np.float32).reshape(4, 4, order='F')

        # Computes the current camera pose by chaining the transformations.
        O_T_C_matrix = np.matmul(O_T_EE_matrix, self.EE_T_C_matrix)

        # Converts the transformation matrix to a pose representation with
        # quaternions.
        O_T_C_pose = np.zeros((7,), dtype=np.float32)
        O_T_C_pose[:3] = O_T_C_matrix[:3, 3]
        O_T_C_pose[3:] = rotation_matrix_to_quaternion(O_T_C_matrix[:3, :3])

        return O_T_C_pose

    def _get_camera_pose_in_map_frame(self):
        M_T_C_pose = self._get_camera_pose_in_base_frame()
        M_T_C_pose[:3] = M_T_C_pose[:3] + self.M_t_M_R
        return M_T_C_pose

    def _initialize_slam(self):
        # Defines the SLAM parameters used for mapping and raycasting.
        # NOTE: Since these parameters will most likely not change, they are
        # hardcoded constants.
        image_width = 640
        image_height = 480
        volume_dimension = 2.0
        volume_resolution = 256
        mu = 0.1

        # TODO: Verify that this calibration actually works correctly.
        k_x = 383.2657775878906
        k_y = 383.2657775878906
        u_0 = 319.3994140625
        v_0 = 242.43833923339844

        # Defines the raycasting parameters.
        self.near_plane = 0.0
        self.far_plane = 2.0

        # Creates the SLAM object that stores the map.
        self.SLAM = DenseSLAMSystemWrapper(image_width=image_width,
                                           image_height=image_height,
                                           volume_dimension=volume_dimension,
                                           volume_resolution=volume_resolution,
                                           mu=mu,
                                           k_x=k_x,
                                           k_y=k_y,
                                           u_0=u_0,
                                           v_0=v_0)

        # Computes the translation between the simulator frame and the map
        # frame M_t_M_C, where R is the Robot frame, M the Map frame and
        # C the Camera frame.
        # This translation is computed as follows:
        # M_t_M_C = M_t_M_R + R_M_R * R_t_R_C, where R_M_R is an identity
        # matrix since the two frames have the same orientation.

        # Defines the map frame origin implicitly by requiring the initial
        # camera position in the simulator frame to lie exactly in the middle
        # of the mapping volume.
        R_t_R_C_init = np.array(self.initial_robot_position,
                                dtype=np.float32)
        M_t_M_C_init = 0.5 * np.array([volume_dimension] * 3, dtype=np.float32)

        # NOTE: All poses passed to the SLAM system must be translated by this
        # vector.
        self.M_t_M_R = M_t_M_C_init - R_t_R_C_init

    def _integrate_into_map(self, depth_image):
        # Integrates the current depth image into the map.
        camera_pose = self._get_camera_pose_in_map_frame()
        if depth_image.dtype != np.float32 or camera_pose.dtype != np.float32:
            raise TypeError('The ndarrays entering the map integration'
                            ' function must be of type np.float32.')
        self.SLAM.integrateIntoMap(depth_image, camera_pose)

    def _raycast_image(self):
        # Raycasts an arbitrarily sized depth image of the 3D reconstruction
        # from a desired camera pose.

        # TODO: For now this raycasts from the same point of view as the camera.
        view_pose = self._get_camera_pose_in_map_frame()

        if view_pose.dtype != np.float32:
            raise TypeError('The pose entering the raycast function must'
                            ' be of type np.float32.')
        # NOTE: The final boolean parameter flips the sign of the computed
        # focal length to transform the simulator camera frame to the SLAM
        # camera frame.
        raycast_image = self.SLAM.raycastFromPose(view_pose, self.image_width,
                                                  self.image_height,
                                                  self.perspective_angle,
                                                  self.near_plane,
                                                  self.far_plane, False)
        return raycast_image

    # TODO: Use the robot state callback to detect errors.
    def robot_state_callback(self, msg):
        self.O_T_EE = msg.O_T_EE
        if not self.received_first_pose:
            self.received_first_pose = True


if __name__ == '__main__':
    rospy.init_node('depth_image_listener', anonymous=False)
    processor = DepthImageProcessor()
    rospy.spin()
