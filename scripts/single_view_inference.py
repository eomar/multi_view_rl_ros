#!/usr/bin/env python


import argparse
import cv2
import cv_bridge
from dqn import DQNAgent
import franka_msgs.msg
import geometry_msgs.msg
from helper_functions import apply_rotation_around_z, compute_quaternion_error
from helper_functions import crop_to_square
from multi_view_rl_ros.srv import CartesianMotionCommand
import numpy as np
import rospy
import sensor_msgs.msg
from supereight_wrapper import depthBlockAveraging
import sys


def rotation_matrix_to_quaternion(R_A_B):
    # Returns the equivalent quaternion in the [q_x, q_y, q_z, q_w] format.
    assert len(R_A_B.shape) == 2
    assert R_A_B.shape[0] == 3
    assert R_A_B.shape[1] == 3
    q_A_B = [None] * 4
    q_A_B[0] = (0.5 * np.sign(R_A_B[2, 1] - R_A_B[1, 2]) *
                np.sqrt(R_A_B[0, 0] - R_A_B[1, 1] - R_A_B[2, 2] + 1))
    q_A_B[1] = (0.5 * np.sign(R_A_B[0, 2] - R_A_B[2, 0]) *
                np.sqrt(R_A_B[1, 1] - R_A_B[2, 2] - R_A_B[0, 0] + 1))
    q_A_B[2] = (0.5 * np.sign(R_A_B[1, 0] - R_A_B[0, 1]) *
                np.sqrt(R_A_B[2, 2] - R_A_B[0, 0] - R_A_B[1, 1] + 1))
    q_A_B[3] = 0.5 * np.sqrt(R_A_B[0, 0] + R_A_B[1, 1] + R_A_B[2, 2] + 1)
    return q_A_B


class DepthImageProcessor(object):
    def __init__(self, args):
        self.cv_bridge = cv_bridge.CvBridge()

        # Initializes subscribers and publishers.
        # NOTE: The queue size is 1 to always use the latest image.
        rospy.Subscriber('/camera/depth/image_rect_raw_throttled',
                         sensor_msgs.msg.Image,
                         self.depth_image_callback, queue_size=1)
        rospy.Subscriber('/franka_state_controller/franka_states',
                         franka_msgs.msg.FrankaState,
                         self.robot_state_callback, queue_size=1)

        self.depth_image_publisher = rospy.Publisher(
            'depth_image', sensor_msgs.msg.Image, queue_size=1)

        # TODO: The different branches (single_64x64 vs. view_placement) have
        # different parameters. Need to keep that in mind when switching
        # between models.

        # Creates a DQNAgent object and loads the desired model to compute
        # desired actions.
        self.agent = DQNAgent(
            state_dim=args['state_dim'],
            image_width=args['image_width'],
            image_height=args['image_height'],
            action_dim=args['action_dim'],
            number_of_actions=args['number_of_actions'],
            initialize_with_sl_model=args['initialize_with_sl_model'],
            freeze_sl_model_weights=args['freeze_sl_model_weights'],
            continue_training=args['continue_training'],
            sl_checkpoint_to_load=args['sl_checkpoint_to_load'],
            sl_model_load_path=args['sl_model_load_path'],
            rl_checkpoint_to_load=args['rl_checkpoint_to_load'],
            rl_model_load_path=args['rl_model_load_path'],
            rl_model_save_path=args['rl_model_save_path'],
            cuda_device=args['cuda_device'],
            evaluation_mode=False,
            learning_rate=args['lr'],
            gamma=args['gamma'],
            epsilon_start=args['eps_start'],
            epsilon_end=args['eps_end'],
            epsilon_decay_tau=args['eps_decay_tau'],
            steps_to_update_target_network=args['steps_to_update_target_network'])

        # The range of distances/angles to move the gripper with when an action
        # is taken.
        # NOTE: Each range is composed of two elements: The minimum and the
        # range of the interval.
        action_movement_distance_range = args['action_movement_distance_range']
        assert len(action_movement_distance_range) == 2
        assert action_movement_distance_range[0] > 0
        assert action_movement_distance_range[1] >= 0
        self.action_movement_distance_range = action_movement_distance_range

        action_rotation_angle_range = args['action_rotation_angle_range']
        assert len(action_rotation_angle_range) == 2
        assert action_rotation_angle_range[0] > 0
        assert action_rotation_angle_range[1] >= 0
        # NOTE: The rotation induced by the action is converted to radians.
        self.action_rotation_angle_range = [
            x * np.pi / 180.0 for x in action_rotation_angle_range
        ]

        # TODO: The actions seem too slow as the gripper approaches, perhaps I
        # can speed them up?

        # Action scaling parameters.
        self.effective_maximum_median_depth = 1.0
        self.effective_minimum_median_depth = 0.25

        # Workspace boundaries.
        self.gripper_min_position_bounds = [0.30, -0.35, 0.15]
        self.gripper_max_position_bounds = [0.65, 0.35, 0.50]

        # Far clipping plane.
        # NOTE: This represents a maximum depth cutoff to have consistent
        # values in the state between the simulation and the real world.
        self.far_clipping_plane = 2.0

        # This is used to prevent the callback from trying to clip the action
        # magnitude before receiving any pose messages.
        self.received_first_pose = False

        # The orientation that aligns the gripper to look down directly at the
        # table (represented in the base frame).
        self.aligned_O_T_EE_quaternion = [-1.0, 0.0, 0.0, 0.0]

        # Depth image filtering mask.
        # TODO: Create a filter manually (through simulation + morphological
        # operations) and load it on startup.
        # NOTE: This is a manually created (and idealized) filter to remove any
        # spurious voxels coming from the peg (or near it).
        self.filtering_mask = np.load(
            '/home/franka/perfect_real_world_filtering_mask.npy')

    def depth_image_callback(self, msg):
        # Integrates the latest depth image into the SLAM map and computes the
        # desired end-effector pose

        # Converts the image message to a float32 numpy array with depth values
        # in meters.
        # NOTE: The input image has the following type: 16UC1.
        depth_image = self.cv_bridge.imgmsg_to_cv2(
            msg).astype(np.float32) * 0.001

        # TODO: Check if this actually ever occurs.

        # Clips any depth values above the far clipping plane used in
        # simulation.
        depth_image[depth_image >
                    self.far_clipping_plane] = self.far_clipping_plane

        # Filters out the peg and the gripper's fingers from the depth image.
        depth_image[self.filtering_mask] = 0.0

        # Crops and downsamples the depth image to 64x64.
        # NOTE: The Realsense developers recommend downsampling with the
        # non-zero block averaging.
        cropped_depth_image = crop_to_square(depth_image, 448)

        # TODO: Why does the depthBlockAveraging not work even though the
        # image is np.float32?
        # downsampled_depth_image = depthBlockAveraging(cropped_depth_image, 7)
        downsampled_depth_image = cropped_depth_image[::7, ::7]

        # Publishes the depth image entering the network for monitoring
        # purposes.
        state_depth_image = (downsampled_depth_image * 1000).astype(np.uint16)
        state_depth_image_msg = self.cv_bridge.cv2_to_imgmsg(state_depth_image)
        self.depth_image_publisher.publish(state_depth_image_msg)

        # Updates the scale of the actions using the median depth value of the
        # main depth image.
        self._scale_action_magnitudes(downsampled_depth_image)

        # Normalizes and flattens the main depth image.
        normalized_depth_image = (downsampled_depth_image /
                                  self.far_clipping_plane)
        normalized_depth_image = normalized_depth_image.reshape((-1, ))

        # Computes the desired action based on the current image.
        action = self.agent.compute_discrete_action(normalized_depth_image)
        axis_of_motion, action_magnitude = self._decode_discrete_action(action)
        print('Current axis of motion: %d, current action magnitude: %f' %
              (axis_of_motion, action_magnitude))

        # Sends a service request to the controller only if the desired action
        # would keep the gripper inside the workspace
        if self._desired_pose_is_in_workspace(axis_of_motion, action_magnitude):
            self._send_command_service(axis_of_motion, action_magnitude)
        else:
            print('Action service request not sent because this action would '
                  'take the gripper outside the workspace.')

    def _decode_discrete_action(self, discrete_action):
        # Transforms the discrete actions (DQN) into a simple set of
        # continuous actions (moving [up, down, etc.] a certain distance).
        # The output consists of the axis of motion and the action magnitude.
        if discrete_action == 0:  # Move up.
            axis_of_motion = 2
            action_magnitude = self.action_movement_distance
        elif discrete_action == 1:  # Move down.
            axis_of_motion = 2
            action_magnitude = -self.action_movement_distance
        elif discrete_action == 2:  # Move front (assume aligned with x-axis).
            axis_of_motion = 0
            action_magnitude = self.action_movement_distance
        elif discrete_action == 3:  # Move back (assume aligned with x-axis).
            axis_of_motion = 0
            action_magnitude = -self.action_movement_distance
        elif discrete_action == 4:  # Move left (assume aligned with y-axis).
            axis_of_motion = 1
            action_magnitude = self.action_movement_distance
        elif discrete_action == 5:  # Move right (assume aligned with y-axis).
            axis_of_motion = 1
            action_magnitude = -self.action_movement_distance
        elif discrete_action == 6:  # Positive rotation around local z-axis.
            axis_of_motion = 3
            action_magnitude = self.action_rotation_angle
        elif discrete_action == 7:  # Negative rotation around local z-axis.
            axis_of_motion = 3
            action_magnitude = -self.action_rotation_angle

        return axis_of_motion, action_magnitude

    def _desired_pose_is_in_workspace(self, axis_of_motion, action_magnitude):
        O_T_EE_matrix = np.array(
            self.O_T_EE, dtype=np.float32).reshape(4, 4, order='F')

        if axis_of_motion != 3:
            # Translation case.
            current_position = O_T_EE_matrix[:3, 3]
            desired_position = current_position[:]
            desired_position[axis_of_motion] = (
                current_position[axis_of_motion] + action_magnitude)

            if desired_position[axis_of_motion] > self.gripper_max_position_bounds[axis_of_motion]:
                return False
            elif desired_position[axis_of_motion] < self.gripper_min_position_bounds[axis_of_motion]:
                return False
        else:
            # Rotation case.
            current_quaternion = rotation_matrix_to_quaternion(
                O_T_EE_matrix[:3, :3])

            # Computes the quaternion error relative to the aligned gripper
            # pose.
            quaternion_error = compute_quaternion_error(
                self.aligned_O_T_EE_quaternion, current_quaternion)
            q_error_z = quaternion_error[2]
            q_error_w = quaternion_error[3]

            # Flips the sign of the quaternion error if the negative version is
            # computed.
            if q_error_w < 0 and q_error_z < 0:
                q_error_w = -q_error_w
                q_error_z = -q_error_z

            elif q_error_w < 0 and q_error_z > 0:
                q_error_w = -q_error_w
                q_error_z = -q_error_z

            # NOTE: This analysis only works when the gripper is rotated purely
            # around the z-axis.
            # NOTE: The Panda arm has slightly stricter bounds (~ 85 degrees).
            # NOTE: The hardcoded values correspond to rotations of +/- 85
            # degrees. (cos(85/2) = ... and sin(85/2) * (0, 0, 1)).
            if q_error_z > 0.58 and q_error_w > 0:
                # In this case, the rotation is more than pi/2.
                return False
            elif q_error_z < -0.58 and q_error_w > 0:
                # In this case, the rotation is less than -pi/2.
                return False

        return True

    # TODO: Use the robot state callback to detect errors.
    def robot_state_callback(self, msg):
        self.O_T_EE = msg.O_T_EE
        if not self.received_first_pose:
            self.received_first_pose = True

    def _scale_action_magnitudes(self, depth_image):
        # Updates the scale of the actions using the median depth value of the
        # current state.
        median_depth = np.median(depth_image)

        # Clips the magnitude of the actions if the median depth falls below
        # the minimum depth or exceeds the effective maximum median depth.
        if median_depth < self.effective_minimum_median_depth:
            self.action_movement_distance = self.action_movement_distance_range[
                0]
            self.action_rotation_angle = self.action_rotation_angle_range[0]

        elif median_depth > self.effective_maximum_median_depth:
            self.action_movement_distance = (
                self.action_movement_distance_range[0] +
                self.action_movement_distance_range[1])
            self.action_rotation_angle = (self.action_rotation_angle_range[0] +
                                          self.action_rotation_angle_range[1])

        else:
            # NOTE: The magnitude of the actions is computed relative to the
            # effective maximum median depth.
            self.action_movement_distance = (
                self.action_movement_distance_range[0] +
                (median_depth - self.effective_minimum_median_depth) *
                (self.action_movement_distance_range[1]) /
                (self.effective_maximum_median_depth -
                 self.effective_minimum_median_depth))

            self.action_rotation_angle = (
                self.action_rotation_angle_range[0] +
                (median_depth - self.effective_minimum_median_depth) *
                (self.action_rotation_angle_range[1]) /
                (self.effective_maximum_median_depth -
                 self.effective_minimum_median_depth))

    def _send_command_service(self, axis_of_motion, action_magnitude):
        rospy.wait_for_service(
            '/cartesian_pose_controller/execute_cartesian_motion')
        try:
            send_desired_action = rospy.ServiceProxy(
                '/cartesian_pose_controller/execute_cartesian_motion',
                CartesianMotionCommand)

            response = send_desired_action(axis_of_motion, action_magnitude)
            return response

        except rospy.ServiceException as e:
            print("Service call failed: %s" % e)


def parse_input_arguments():
    """Helper function to parse the many input arguments of the RL setup."""
    parser = argparse.ArgumentParser(
        description='Arguments to simulate and train a robot arm for manipulation.')

    # TODO: Make sure to expand this set of parameters when switching to the
    # multi-view models.

    # MDP parameters.
    parser.add_argument('--state-dim',
                        help='Dimension of the state',
                        default=12288,
                        type=int)
    parser.add_argument('--image-width',
                        help='Width of the images entering the network',
                        default=64,
                        type=int)
    parser.add_argument('--image-height',
                        help='Height of the images entering the network',
                        default=64,
                        type=int)
    parser.add_argument('--action-dim',
                        help='Dimension of the action',
                        default=1,
                        type=int)
    parser.add_argument('--number-of-actions',
                        help='Number of available actions',
                        default=8,
                        type=int)

    # Network and RL Hyperparameters.
    parser.add_argument('--lr',
                        help='Network learning rate',
                        default=0.001,
                        type=float)
    parser.add_argument('--gamma',
                        help='Discount factor',
                        default=0.99,
                        type=float)
    parser.add_argument('--batch-size',
                        help='Batch size',
                        default=64,
                        type=int)
    parser.add_argument('--eps-start',
                        help='Epsilon starting value',
                        default=1.0,
                        type=float)
    parser.add_argument('--eps-end',
                        help='Epsilon ending value',
                        default=0.1,
                        type=float)
    parser.add_argument('--eps-decay-tau',
                        help='Epsilon decay time constant in timesteps [t]',
                        default=10000,
                        type=int)
    parser.add_argument('--steps-to-update-target-network',
                        help='Number of steps to update the target network',
                        default=1000,
                        type=int)

    # Training parameters.
    parser.add_argument('--cuda-device',
                        help='The cuda device to use for training',
                        default=0,
                        type=int)

    # Action scaling parameters.
    parser.add_argument('--action-movement-distance-range',
                        help='The minimum and the range of the action '
                        'movement distance interval (min + range = max) '
                        'in [m]',
                        nargs=2,
                        default=[0.005, 0.045],
                        type=float)
    parser.add_argument('--action-rotation-angle-range',
                        help='The minimum and the range of the action '
                        'rotation angle interval (min + range = max) in [deg]',
                        nargs=2,
                        default=[2.0, 11.0],
                        type=float)

    # Model initialization and saving/loading parameters.
    parser.add_argument('--initialize-with-sl-model',
                        help='Flag to initialize the RL model with the '
                        'weights of a trained SL model',
                        action='store_true')
    parser.add_argument('--freeze-sl-model-weights',
                        help='Flag to freeze the trained SL model weights',
                        action='store_true')
    parser.add_argument('--continue-training',
                        help='Flag to continue training with the '
                        'epsilon from the saved model',
                        action='store_true')
    parser.add_argument('--sl-checkpoint-to-load',
                        help='Checkpoint number to load',
                        default=0,
                        type=int)
    parser.add_argument('--sl-model-load-path',
                        help='The load path of the SL model relative '
                        'to the current directory',
                        default='../sl_models/insert_model_name_here',
                        type=str)
    parser.add_argument('--rl-checkpoint-to-load',
                        help='Checkpoint number to load',
                        default=0,
                        type=int)
    parser.add_argument('--rl-model-load-path',
                        help='The load path of the RL model relative '
                        'to the current directory',
                        default=None,
                        type=str)
    parser.add_argument('--rl-model-save-path',
                        help='The save path of the RL model relative '
                        'to the current directory',
                        default='../models/insert_model_name_here',
                        type=str)

    args = vars(parser.parse_args(sys.argv[5:]))
    return args


if __name__ == '__main__':
    rospy.init_node('depth_image_listener', anonymous=False)
    args = parse_input_arguments()
    processor = DepthImageProcessor(args)
    rospy.spin()
